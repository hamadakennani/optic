# -*- coding: utf-8 -*-
###################################################################################
#    A part of Open HRMS Project <https://www.openhrms.com>
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2018-TODAY Cybrosys Technologies (<https://www.cybrosys.com>).
#    Author: Niyas Raphy(<https://www.cybrosys.com>)
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################
{
    'name': 'Gestion Optique',
    'version': '11.0.1.0.1',
    'summary': 'Gestion Optique',
    'author': 'INNOVUPTECH',
    'company': 'INNOVUPTECH',
    'website': 'https://www.innovuptech.com',
    'depends': [],
    'category': 'Optic',
    'maintainer': 'INNOVUPTECH',
    'demo': [],
    'data': [
        # 'security/security.xml',
        'security/ir.model.access.csv',
        # 'data/resign_employee.xml',
        # 'views/hr_employee.xml',
        'views/config/marque_view.xml',
        'views/config/ville_view.xml',
        'views/config/mutuelle_view.xml',
        'views/config/banque_view.xml',
        'views/config/reglement_view.xml',
        'views/config/matiere_view.xml',
        'views/config/type_article_view.xml',
        'views/config/type_depense_view.xml',
        'views/config/type_montage_view.xml',
        'views/config/couleur_view.xml',
        'static/xml/data.xml',
        'views/menu/menu_parametre_view.xml',

    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': [],
    'license': 'AGPL-3',
}

