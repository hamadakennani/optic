import datetime
from datetime import datetime
from odoo import models, fields, api,tools
from odoo.exceptions import ValidationError


class Marque(models.Model):
    _name = 'optic.config.marque'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)
    description = fields.Text(string='Description')


class Ville(models.Model):
    _name = 'optic.config.ville'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)
    pays = fields.Char(string='Pays', required=True)


class Mutuelle(models.Model):
    _name = 'optic.config.mutuelle'
    _rec_name = 'intitule'

    code = fields.Char(string='Code', required=True)
    intitule = fields.Char(string='Intitulé', required=True)


class Banque(models.Model):
    _name = 'optic.config.banque'
    _rec_name = 'intitule'

    code = fields.Char(string='Code', required=True)
    intitule = fields.Char(string='Intitulé', required=True)


class Reglement(models.Model):
    _name = 'optic.config.reglement'
    _rec_name = 'intitule'

    code = fields.Char(string='Code', required=True)
    intitule = fields.Char(string='Intitulé', required=True)


class Matiere(models.Model):
    _name = 'optic.config.matiere'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)
    typeArticle = fields.Many2one('optic.config.type_article',string='Type article', required=True)


class TypeArticle(models.Model):
    _name = 'optic.config.type_article'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)


class TypeArticle(models.Model):
    _name = 'optic.config.couleur'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)


class TypeArticle(models.Model):
    _name = 'optic.config.type_montage'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)


class TypeArticle(models.Model):
    _name = 'optic.config.type_depense'
    _rec_name = 'intitule'

    intitule = fields.Char(string='Intitulé', required=True)
